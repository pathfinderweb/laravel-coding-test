<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Voucher;
use Illuminate\Validation\ValidationException;

class VoucherApiController extends Controller
{
    private function randomDateTime()
    {
        //Generate a timestamp using mt_rand.
        $timestamp = mt_rand(strtotime('-3 days'), strtotime('+3 days'));

        //Format that timestamp into a readable date string.
        $randomDate = date("Y-m-d H:i:s", $timestamp);

        return $randomDate;
    }

    public function generateVoucherCodes(Request $request)
    {
        $alphaNum = '';
        $voucherArray = array();
        $time = -microtime(true);
    
        for ($i=0; $i<1000000; $i++) {

            $alphaNum = (string) dechex($i);
            $strLen = strlen($alphaNum);

            if ($strLen < 6) {
                if ($i % 2) {
                    $alphaNum = Str::padLeft($alphaNum, 6, 'zyx');

                } else {
                    $alphaNum = Str::padRight($alphaNum, 6, 'xzz');
                }
                
            }

            $expiryDate = $this->randomDateTime();
            $dateNow = date("Y-m-d H:i:s");
            $voucherStatus = $expiryDate < $dateNow ? 'Expired' : 'Available';
            $voucherArray[$i] = [
                'voucher_code' => $alphaNum,
                'status' => $voucherStatus,
                'expiry_date' => $expiryDate
            ];
        }

        $chunks = array_chunk($voucherArray, 1000);

        foreach ($chunks as $chunk) {
            shuffle($chunk);
            DB::table('vouchers')->insert($chunk);
        }

        $time += microtime(true);

        $response = [
            'totalTime' => number_format($time, 3, '.', ''),
            'status' => 'success'
        ];

        //response back to request
        return $this->respondSuccess($response);
    }

    public function getAvailableVoucher(Request $request)
    {
        $response = [
            'voucher_code' => null
        ];

        $voucherObj = Voucher::whereStatus('Available')->first();

        if ($voucherObj) {
            data_set($response, 'voucher_code', $voucherObj->voucher_code);
        }

        //response back to request
        return $this->respondSuccess($response);
    }

    public function claimVoucher(Request $request)
    {
        $rules = [];
        $rules = [
            'code' => 'required|alpha_num|min:6|max:6'
        ];

        $requestData = $request->all();

        $message = [
            'code.digits' => "Voucher 'code' must be 6 characters"
        ];

        $validator = Validator::make($request->all(), $rules, $message);
 
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        //Look up for the voucher
        $voucherObj = Voucher::where('voucher_code', data_get($requestData, 'code'))->first();

        if ($voucherObj) {
            if ($voucherObj->status == 'Available' && $voucherObj->status != 'Expired') {
                $voucherObj->status = 'Claimed';
                $voucherObj->save();

                $response = ['message' => 'Claim Successful.'];

            } else {
                $response = ['message' => 'Voucher already been claimed before.'];
            }

        } else {
            return $this->respondNotFound('Voucher not found with provided code.');
        }

        //response back to request
        return $this->respondSuccess($response);
    }

    public function getStatistic(Request $request)
    {
        //Numbers of Available, Claimed and Expired
        $availableCount = Voucher::whereStatus('Available')->count();
        $claimedCount = Voucher::whereStatus('Claimed')->count();
        $expiredCount = Voucher::whereStatus('Expired')->count();

        //Numbers of Expiring in 6, 12, 24 hours.
        $now = now();
        $next6H = date('Y-m-d H:i:s', strtotime('+6 hours'));
        $next12H = date('Y-m-d H:i:s', strtotime('+12 hours'));
        $next24H = date('Y-m-d H:i:s', strtotime('+24 hours'));

        $expiringIn6H = Voucher::whereStatus('Available')->whereBetween('expiry_date', [$now, $next6H])->count();
        $expiringIn12H = Voucher::whereStatus('Available')->whereBetween('expiry_date', [$now, $next12H])->count();
        $expiringIn24H = Voucher::whereStatus('Available')->whereBetween('expiry_date', [$now, $next24H])->count();

        $response = [
            'available' => $availableCount,
            'claimed' => $claimedCount,
            'expired' => $expiredCount,
            'expiring_in_6_hours' => $expiringIn6H,
            'expiring_in_12_hours' => $expiringIn12H,
            'expiring_in_24_hours' => $expiringIn24H,
        ];

        //response back to request
        return $this->respondSuccess($response);
    }
}
