FROM ubuntu:18.04
ENV DEBIAN_FRONTEND noninteractive

# Install packages
ADD provision.sh /provision.sh
ADD serve.sh /serve.sh

ADD supervisor.conf /etc/supervisor/conf.d/supervisor.conf

RUN chmod +x /*.sh

RUN ./provision.sh

COPY --chown=homestead:homestead . /var/www/html/

EXPOSE 80 443 22 6379
CMD ["/usr/bin/supervisord"]