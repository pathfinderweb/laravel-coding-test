<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\VoucherApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('generateVoucherCodes', [VoucherApiController::class, 'generateVoucherCodes']);
Route::get('getAvailableVoucher', [VoucherApiController::class, 'getAvailableVoucher']);
Route::get('claim', [VoucherApiController::class, 'claimVoucher']);
Route::get('getStatistic', [VoucherApiController::class, 'getStatistic']);