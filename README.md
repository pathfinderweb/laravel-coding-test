## Build docker image & save to local
```shell
docker image build --no-cache --tag=laravel-coding-test .
```

## Start your containers
```shell
docker compose up -d
```

## SSH into the container (password: secret):
```shell
ssh -p 2222 homestead@localhost
```

**In the host**, update ``` /etc/hosts ``` to include your app domain:
```shell
127.0.0.1               localhost
```


### API Endpoints
```shell
http://localhost/api/generateVoucherCodes
```

```shell
http://localhost/api/getAvailableVoucher
```

```shell
http://localhost/api/claim?code=<code>
```

```shell
http://localhost/api/getStatistic
```

### Notes
Set Header as below:
```shell
Content-Type = application/json
Accept = application/json
```